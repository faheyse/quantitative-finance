# Knock-out call option payoff
# Using Call options as a primitive 

from matplotlib import pyplot as plt
import numpy as np

class Call:
    def __init__(self, S, k):
        self.S = S
        self.k = k
    def payoff(self):
        return max(self.S - self.k, 0)
    def __repr__(self):
        return "Call(S = {}, k = {})".format(self.S, self.k)
    def __str__(self):
        return "Call(S = {}, k = {})".format(self.S, self.k)

# Digital call as a fucntion of 2 Calls
class DigitalCall:
    n = 1 # payoff of the digital call
    def __init__(self, S, k):
        self.S = S
        self.k = k
        self.l = 100000
        self.lower = Call(self.S, self.k)
        self.upper = Call(self.S, self.k + 1/self.l)
    def payoff(self):
        return self.n*round(self.l*(self.lower.payoff() - self.upper.payoff()))
    def set_pay(self, n):
        self.n = n
    def __str__(self):
        return "DigitalCall(l = {}, S = {}, k = {})".format(self.l, self.S, self.k)


# Knock-out call as a function of 2 Calls and 1 Digital
class KnockOutCall(Call, DigitalCall):
    def __init__(self, S, k, B):
        self.S = S
        self.k = k
        self.B = B
        self.D = DigitalCall(S, B)
        self.D.set_pay(B-k)
        self.Ck = Call(S, k)
        self.CB = Call(S, B)
    def payoff(self):
        return - self.D.payoff() + self.Ck.payoff() - self.CB.payoff() 
    def __str__(self):
        return "KnockOutCall(S = {}, k = {}, B = {})".format(self.S, self.k, self.B)



# plot payoff vs price of underlying asset S
S = np.linspace(0, 100, 100)
k = 50
B = 80
C = [KnockOutCall(i,k,B).payoff() for i in S]
plt.plot(S, C)
plt.xlabel("S")
plt.ylabel("Payoff")
plt.title("Knock-out call option payoff")
plt.show()
