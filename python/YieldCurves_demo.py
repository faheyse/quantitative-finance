import matplotlib.pyplot as plt
import numpy as np

# Define the range of maturities
maturities = np.linspace(1, 30, 30)

# Define the yield curves
normal_yield = 0.02 + 0.01 * np.sqrt(maturities)
inverted_yield = 0.06 - 0.01 * np.sqrt(maturities)
humped_yield = 0.02 + 0.015 * np.sin(maturities / 6)

# Create the plot
plt.figure(figsize=(10, 6))

# Plot the yield curves
plt.plot(maturities, normal_yield, label='Normal Yield Curve')
plt.plot(maturities, inverted_yield, label='Inverted Yield Curve')
plt.plot(maturities, humped_yield, label='Humped Yield Curve')

# Add labels and title
plt.xlabel('Maturity (Years)')
plt.ylabel('Yield')
plt.title('Yield Curves')
plt.legend()

# Show the plot
plt.show()
