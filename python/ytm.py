import sympy as sp

c = 0.03375
m = 2
n=20
F=100
C=c*F/m

P=95

r = sp.symbols('r')

PV = sum([C/(1+r/m)**i for i in range(1, n+1)]) + F/(1+r/m)**n
PVprime = sp.diff(PV, r)
# print(PV.evalf(subs={r:0.01}))


def newton_raphson(f, df, x0, epsilon, max_iter):
    xn = x0
    for n in range(0, max_iter):
        fxn = f.evalf(subs={r:xn})
        if abs(fxn) < epsilon:
            print('Found solution after',n,'iterations.')
            return xn
        dfxn = df.evalf(subs={r:xn})
        if dfxn == 0:
            print('Zero derivative. No solution found.')
            return None
        xn = xn - fxn/dfxn
    print('Exceeded maximum iterations. No solution found.')
    return None


x0 = 0.01 # Initial guess
epsilon = 1e-10 # Error tolerance
max_iter = 500 # Maximum number of iterations
f = PV-P # Function
root = newton_raphson(f, PVprime, x0, epsilon, max_iter)
print('yield to maturity at price of ${0}: '.format(P), root)
