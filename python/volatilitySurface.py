from sqlalchemy import create_engine, MetaData, Table, select
import pandas as pd
import numpy as np
from scipy.interpolate import griddata
from scipy.optimize import minimize_scalar
from scipy.stats import norm, skew, kurtosis
import matplotlib.pyplot as plt
from datetime import datetime
# import yfinance as yf


def get_option_data(engine, table):
    query = select(
        table.c.strike,
        table.c.bid,
        table.c.ask,
        table.c.expirationdate
    ).where(table.c.inthemoney == False)

    with engine.connect() as connection:
        result = connection.execute(query).fetchall()

    data = pd.DataFrame(result, columns=['strike', 'bid', 'ask', 'expirationdate'])
    # convert to floating point
    for col in data.columns:
        if col != 'expirationdate':
            data[col] = data[col].astype(float)

    return data

def clean_and_transform_data(data, mean_spread, today):
    data.dropna(subset=['market_price'], inplace=True)
    data['maturity'] = pd.to_datetime(data['expirationdate'])
    data['maturity'] = (data['maturity'] - today).dt.days
    data = data.astype({'maturity': 'float', 'strike': 'float', 'market_price': 'float'})
    return data.drop(columns=['bid', 'ask'])


def calculate_implied_volatility(row, stock_price, risk_free_rate):
    result = minimize_scalar(
        loss_function,
        bounds=(0.001, 5),
        args=(
            stock_price,
            row['strike'],
            row['maturity'] / 365,
            risk_free_rate,
            row['market_price']
        ),
        method='bounded'
    )
    return result.x


def loss_function(sigma, S, K, T, r, market_price):
    return (black_scholes_call(S, K, T, r, sigma) - market_price) ** 2


def black_scholes_call(S, K, T, r, sigma):
    d1 = (np.log(S / K) + (r + 0.5 * sigma ** 2) * T) / (sigma * np.sqrt(T))
    d2 = d1 - sigma * np.sqrt(T)
    return S * norm.cdf(d1) - K * np.exp(-r * T) * norm.cdf(d2)


def plot_volatility_surface(data, today):
    pivot_data = data.pivot(index='maturity', columns='strike', values='impliedvolatility')
    X, Y = np.meshgrid(pivot_data.columns, pivot_data.index)
    Z = pivot_data.values

    valid_idx = ~np.isnan(Z)
    x_valid = X[valid_idx]
    y_valid = Y[valid_idx]
    z_valid = Z[valid_idx]


    xi, yi = np.linspace(X.min(), X.max(), 500), np.linspace(Y.min(), Y.max(), 500)
    xi, yi = np.meshgrid(xi, yi)

    try:
        zi = griddata((x_valid, y_valid), z_valid, (xi, yi), method='cubic')  
    except Exception as e:
        print("Error during interpolation:", e)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.plot_surface(xi, yi, zi, cmap='viridis')
    ax.set_xlabel('Maturity')
    ax.set_ylabel('Strike')
    ax.set_zlabel('Implied Volatility')
    ax.set_title(f'Surface Plot on {today}')
    plt.show()


def main():
    today = datetime(2023, 9, 6)
    # stock = yf.Ticker("NVDA")
    # stock_price = stock.history(start="2023-09-06", end="2023-09-07")["Close"].iloc[0]
    stock_price = 460.00
    risk_free_rate = 0.05

    db_params = {
        'user': 'seanf',
        'password': '',
        'host': 'localhost',
        'port': '5432',
        'database': 'marketdb'
    }

    engine = create_engine(f"postgresql://{db_params['user']}:{db_params['password']}@{db_params['host']}:{db_params['port']}/{db_params['database']}")
    metadata = MetaData()
    table = Table('NVDA_CALL_CHAIN', metadata, autoload_with=engine)

    data = get_option_data(engine, table)
    mean_spread = (data['ask'] - data['bid']).mean()
    data['market_price'] = np.nan
    data.loc[(~data['bid'].isna()) & (~data['ask'].isna()), 'market_price'] = (data['bid'] + data['ask']) / 2
    data.loc[(data['bid'].isna()) & (~data['ask'].isna()), 'market_price'] = data['ask'] - mean_spread
    data.loc[(~data['bid'].isna()) & (data['ask'].isna()), 'market_price'] = data['bid'] + mean_spread

    data = clean_and_transform_data(data, mean_spread, today)
    data['impliedvolatility'] = data.apply(calculate_implied_volatility, args=(stock_price, risk_free_rate), axis=1)

    print("Basic Statistics:")
    print(data['impliedvolatility'].describe())

    skewness = skew(data['impliedvolatility'])
    kurt = kurtosis(data['impliedvolatility'])
    print("\nAdditional Statistics:")
    print(f"Skewness: {skewness}")
    print(f"Kurtosis: {kurt}")

    plot_volatility_surface(data, today.strftime("%Y-%m-%d"))


if __name__ == "__main__":
    main()
