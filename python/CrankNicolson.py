# Crank-Nicolson method for pricing a knock-out barrier call option - demo


import numpy as np

sigma = 0.2     # Volatility
r = 0.05        # Risk-free interest rate
S_max = 100     # Maximum asset price
B = 80          # Barrier
K = 50          # Strike
T = 1
M = 100         # Number of asset price steps
N = 100         # Number of time steps

dS = S_max / M  # Asset price step
dt = T / N      # Time step

# Initialize option price grid
V = np.zeros((M+1, N+1))
V[:, N] = np.maximum(np.arange(0, S_max + dS, dS) - K, 0)

# Iterate through the grid (backward in time)
for n in range(N-1, -1, -1):
    A_matrix = np.zeros((M-1, M-1))    
    B_matrix = np.zeros((M-1, M-1))
    d_vector = np.zeros(M-1)
    
    for i in range(1, M):
        ai = 0.25 * dt * (sigma**2 * i**2 - r * i)
        bi = -0.5 * dt * (sigma**2 * i**2 + r)
        ci = 0.25 * dt * (sigma**2 * i**2 + r * i)
        
        A_matrix[i-1, i-1] = 1 - bi
        B_matrix[i-1, i-1] = 1 + bi
        if i > 1:
            A_matrix[i-1, i-2] = -ai
            B_matrix[i-1, i-2] = ai
        if i < M-1:
            A_matrix[i-1, i] = -ci
            B_matrix[i-1, i] = ci
        d_vector[i-1] = V[i, n+1]
    
    # Solve the system of equations
    V[1:M, n] = np.linalg.solve(A_matrix, B_matrix @ d_vector)

    # Boundary condition at barrier (nearest to B)
    i_barrier = int(B / dS)
    V[i_barrier, n] = 0

    # Boundary condition at S = 0
    V[0, n] = 0

    # Boundary condition at S = S_max
    V[M, n] = 2 * V[M-1, n] - V[M-2, n]

# Desired underlying asset price
S_current = 60
i_current = int(S_current / dS)

# Option price at the current underlying asset price and time t = 0
option_price = V[i_current, 0]

print(f"Option price: {option_price:.2f}")
