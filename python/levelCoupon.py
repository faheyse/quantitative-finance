from matplotlib import pyplot as plt
import matplotlib
import numpy as np

matplotlib.style.use('ggplot')

# each element in flows_r is a list of cash flows for a given rate
flows_r = []

# calculate the cash flows for a given coupon rate
def PV_flows(c):
    flows = []

    # Par value
    F = 10000

    # risk-free rate
    r = 0.05

    # number of payments per year
    m = 2

    # Cash flow per period 
    C = F*c/m

    f = 0 
    for i in range(1, 11):
        f += C/(1+r/m)**i
        flows.append(f + F/(1+r/m)**i)

    flows.reverse()
    return flows



# rates of interest
rates = np.array([2, 5, 10])/100

# calculate the cash flows for each coupon
for c in rates:
    flows_r.append(PV_flows(c))


plt.plot(flows_r[0], label='2%')
plt.plot(flows_r[1], label='5%')
plt.plot(flows_r[2], label='10%')
plt.xlabel('Time')
plt.ylabel('Present Value (Dollars)')
plt.title('Present Value of Cash Flows')
plt.legend()
plt.show()

