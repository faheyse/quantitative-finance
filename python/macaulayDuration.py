# Macaulay Duration

# FV = Face Value
# c = annual coupon rate
# r = yield to maturity rate annualized
# m = number of coupon payments per year
# n = total number of cash flows


def levelCouponBond(FV=100, c=0.03, r=0.05, m=2, n=20):
    C = FV*c/m
    return sum([C/(1+r/m)**i for i in range(1, n+1)]) + FV/(1+r/m)**n

def MD(FV=100, c=0.03, r=0.05, m=2, n=20):
    C = FV*c/m
    P = levelCouponBond()
    md = 1/P * ( sum([i*C/(1+r/m)**i for i in range(1, n+1)]) + n*FV/(1+r/m)**n )
    md_years = md/m
    return md_years

print(MD())
