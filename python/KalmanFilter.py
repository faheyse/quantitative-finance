# This script works with the PostgreSQL database to fetch stock data

import numpy as np
import matplotlib.pyplot as plt
import os
from time import sleep
from sqlalchemy import create_engine, select, Table, MetaData


# Connect to PostgreSQL
engine = create_engine("postgresql://seanf:@127.0.0.1:5432/marketdb")
connection = engine.connect()

# Initialize Kalman Filter Parameters
initial_state = 0
state_estimate = initial_state
state_covariance = 1
process_variance = 0.1  # Q
measurement_variance = 0.1  # R
kalman_gain = 0

# Fetch Data from PostgreSQL
table_name = "AAPL"
meta = MetaData()
table = Table(table_name, meta, autoload_with=engine)
query = select(table.c.close).order_by(table.c.date)
result = connection.execute(query)
data = result.fetchall()

# Close connection
connection.close()

# for prediction plotting
predictions = []

# Kalman Filter Loop
for measurement in data:

    # Prediction Step
    predicted_state_estimate = state_estimate
    predicted_state_covariance = state_covariance + process_variance
    predictions.append(predicted_state_estimate)

    # Update Step
    kalman_gain = predicted_state_covariance / (predicted_state_covariance + measurement_variance)
    state_estimate = predicted_state_estimate + kalman_gain * (measurement[0] - predicted_state_estimate)
    state_covariance = (1 - kalman_gain) * predicted_state_covariance

    # Your prediction is in state_estimate
    # print(f"Predicted closing price: {state_estimate}")

#plot 
plt.plot(data[1:], label='Actual')
plt.plot(predictions[1:], label='Predicted')
plt.xlabel('Days')
plt.ylabel('Closing Price')
plt.title('Apple Inc. Stock Price Prediction')
plt.legend()
plt.show()
