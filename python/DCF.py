import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

cash_flow = 100.0
values = []
def dcf(cash_flow: float, r: int):
    v = 0 
    for i in range(100):
        v = v + cash_flow/((1 + r/100)**i)
        values.append(v)
    return v

print(round(dcf(cash_flow, 10), 2))

# plot values
plt.plot(values)
plt.ylabel('Value')
plt.xlabel('Periods')
plt.title('Discounted Cash Flow')
plt.show()

# chart shows that the value asymptotically approaches a value of 1000 + 1000/0.1 = 1100
