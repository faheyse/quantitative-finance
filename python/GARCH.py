import pandas as pd
from arch import arch_model

# 1. Import necessary libraries
# 2. Load the VOO.csv data
data = pd.read_csv('data/VOO.csv')

# 3. Calculate daily returns
data['returns'] = 100 * data['Close'].pct_change().dropna()

# Removing NaN values
data = data.dropna()

# 4. Set up and fit the GARCH(1,1) model
model = arch_model(data['returns'], vol='Garch', p=1, q=1)
results = model.fit(update_freq=5)

# 5. Forecast 30-day volatility
forecast = results.forecast(start=0, horizon=30)

# Average variance over the 30-day forecasted period
average_daily_variance = forecast.variance.iloc[-1].mean()

# Approximate total volatility over the 30 days
total_volatility_30d = (30 * average_daily_variance) ** 0.5

print(total_volatility_30d)

