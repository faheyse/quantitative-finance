"""
    MONTE CARLO SIMULATION: METROPOLIS-HASTINGS ALGORITHM

    Parameters:
    - target_pdf: The target probability density function we want to sample from.
    - proposal_pdf: The proposal probability density function.
    - proposal_sample: Function to generate a sample from the proposal distribution.
    - num_samples: Number of samples to generate.
    - start_position: Starting position of the Markov chain.

    Returns:
    - samples: Array of samples from the target distribution.
"""
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

def metropolis_hastings(target_pdf, proposal_pdf, proposal_sample, num_samples, start_position, burn_in, thinning_factor):

    current_position = start_position
    samples = []
    accepted = 0

    for i in range(num_samples + burn_in):
        # Propose a new sample
        proposed_position = proposal_sample(current_position)

        # calculate acceptance ratio
        acceptance_ratio = min(1, (target_pdf(proposed_position) * proposal_pdf(current_position, proposed_position)) /
                                  (target_pdf(current_position) * proposal_pdf(proposed_position, current_position)))

        # accept or reject the proposed sample
        if np.random.uniform(0, 1) < acceptance_ratio:
            current_position = proposed_position
            accepted += 1

        # use burn-in and thinning
        if i >= burn_in and (i - burn_in) % thinning_factor == 0:
            samples.append(current_position)

    acc_rate = accepted / num_samples

    return np.array(samples), acc_rate

if __name__ == "__main__":

    # target distribution
    def target_distribution(x):
        return 0.3 * np.exp(-0.2 * (x - 3)**2) + 0.7 * np.exp(-0.8 * (x + 2)**2)

    # Gaussian proposal dist
    # proposal_distribution and proposal_sample should use the same distribution. Here we use a Gaussian distribution.
    SIGMA = 3.7
    def proposal_distribution(x, x_prime, sigma=SIGMA):
        return norm.pdf(x_prime, loc=x, scale=sigma)

    def proposal_sample(x, sigma=SIGMA):
        return x + sigma * np.random.normal()

    # Generate samples
    # burn-in: number of samples to discard before starting to collect samples
    # thinning_factor: number of samples to discard between each sample collected
    samples, acc_rate = metropolis_hastings(target_distribution, proposal_distribution, proposal_sample, 100000, 0, burn_in=1000, thinning_factor=10)


    print("Normalized Mean:", np.mean(samples))
    print("Standard Deviation:", np.std(samples))
    print("Acceptance Rate:", acc_rate) # should be between 0.2 and 0.5

    x = np.linspace(-7, 7, 1000)
    plt.hist(samples, bins=100, density=True, alpha=0.6, label="Samples")
    plt.plot(x, target_distribution(x), 'r', label="Target Distribution")
    plt.legend()
    plt.show() 

