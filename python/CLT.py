import numpy as np
import matplotlib.pyplot as plt

# Number of random samples
num_samples = 1000

# Size of each sample
sample_size = 1000

# Create an empty list to hold the sample means
sample_means = []

# Generate the samples and compute the means
for _ in range(num_samples):
    # Generate a sample of 500 random numbers from any distribution
    samples = np.random.laplace(loc=0, scale=1, size=sample_size)
    sample_means.append(np.mean(samples))


# Plot the histogram of the sample means
plt.hist(sample_means, bins=30, density=True)
plt.xlabel('Sample Means')
plt.ylabel('Frequency')
plt.title('Demonstration of Central Limit Theorem')
plt.show()
