# Exercise: Monte Carlo simulation of delta
# Comparison of Monte Carlo simulation and direct calculation of delta

import numpy as np
from sympy import symbols, exp, sqrt, log, pi, erf

# Parameters
S_0 = 100  # Initial price
K = 110    # Strike
T = 1      # Time to expiration (in years)
r = 0.05   
sigma = 0.2 # Volatility
d = 0.001  # Small change in stock price
n_simulations = 100000 



# delta using Monte Carlo simulation

# Keep same values across simulations
np.random.seed(42) 
Z = np.random.normal(0, 1, n_simulations)
def monte_carlo_call_price(S, K, T, r, sigma, n_simulations):
    S_T = S * np.exp((r - 0.5 * sigma**2) * T + sigma * np.sqrt(T) * Z)
    payoff = np.maximum(S_T - K, 0)
    call_price = np.exp(-r * T) * np.mean(payoff)
    return call_price

# Calculate call prices for S_0 and S_0 * (1 + d) in one step
call_price_S_0 = monte_carlo_call_price(S_0, K, T, r, sigma, n_simulations)
call_price_S_0_d = monte_carlo_call_price(S_0 * (1 + d), K, T, r, sigma, n_simulations)

# Calculate delta
delta = (call_price_S_0_d - call_price_S_0) / (S_0 * d)

print(f"MCS simulation: {delta}")



# direct calculation of delta using differentiation

# Symbols
S, K_sym, T_sym, r_sym, sigma_sym = symbols('S K T r sigma', real=True, positive=True)

# Black-Scholes d1 and d2
d1 = (log(S / K_sym) + (r_sym + 0.5 * sigma_sym**2) * T_sym) / (sigma_sym * sqrt(T_sym))
d2 = d1 - sigma_sym * sqrt(T_sym)

# Cumulative standard normal distribution function
def cdf(x):
    return (1 + erf(x / sqrt(2))) / 2

# Black-Scholes call option price
call_price = S * cdf(d1) - K_sym * exp(-r_sym * T_sym) * cdf(d2)

# Differentiate call price with respect to S to get delta
delta_expr = call_price.diff(S)

# Substitute the values into the expression
delta_value = delta_expr.subs({S: S_0, K_sym: K, T_sym: T, r_sym: r, sigma_sym: sigma})

print(f"Direct calculation: {delta_value.evalf()}")







