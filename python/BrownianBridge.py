import numpy as np
import matplotlib.pyplot as plt

# Parameters
S = 0  # Starting point
E = 1  # Ending point
n = 1000  # Number of intermediate points
t0 = 0  # Start time
tn = 1  # End time

# Time intervals
time_intervals = np.linspace(t0, tn, n+1)
dt = (tn - t0) / n

# Generate standard Brownian motion
B_t = np.sqrt(dt) * np.random.randn(n+1)    # sqrt(dt) because (dB_t)^2 = dt
B_t[0] = 0
B_t = np.cumsum(B_t)


# Calculate the three terms independently
term1 = B_t
term2 = ((time_intervals - t0) / (tn - t0)) * (B_t[-1] - B_t[0])
term3 = ((time_intervals - t0) / (tn - t0)) * (E - S)

# Construct the Brownian Bridge
BB_t = term1 - (term2 - term3)

# Plot the Brownian Bridge
plt.plot(time_intervals, BB_t, label='Brownian Bridge')
plt.plot(time_intervals, term1, label='Random Walk')
plt.plot(time_intervals, term2, label='Slope of Random Walk')
plt.plot(time_intervals, term3, label='Slope of Endpoints')
plt.xlabel('Time')
plt.ylabel('Value')
plt.title('Sample Path of Brownian Bridge')
plt.plot([t0, tn], [S, E], 'ro', label='Starting and Ending Points')
plt.legend()
plt.show()
