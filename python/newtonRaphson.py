import sympy as sp

def newton_raphson(f, df, x0, epsilon, max_iter):
    xn = x0
    for n in range(0, max_iter):
        fxn = f.evalf(subs={x:xn})
        if abs(fxn) < epsilon:
            print('Found solution after',n,'iterations.')
            return xn
        dfxn = df.evalf(subs={x:xn})
        if dfxn == 0:
            print('Zero derivative. No solution found.')
            return None
        xn = xn - fxn/dfxn
    print('Exceeded maximum iterations. No solution found.')
    return None

x = sp.symbols('x')
f = x**5 - 3*x**4 + 2*x**3 - x**2 + x - 1
df = sp.diff(f, x)
x0 = 2
epsilon = 1e-10
max_iter = 500

root = newton_raphson(f, df, x0, epsilon, max_iter)
print('Root is at: ', root)
