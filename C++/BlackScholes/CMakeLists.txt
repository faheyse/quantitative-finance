cmake_minimum_required(VERSION 3.10)

# set the project name
project(BlackScholes)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 14)

# find the QuantLib package
find_package(QuantLib CONFIG REQUIRED)

# add the executable
add_executable(BlackScholes main.cpp)

# link the QuantLib library
target_link_libraries(BlackScholes PRIVATE QuantLib::QuantLib)
