#!/bin/bash

rm build/CMakeCache.txt
rm build/cmake_install.cmake
rm build/Makefile
rm -rf build/CMakeFiles
rm build/BlackScholes