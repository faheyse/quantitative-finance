/*
Black-Scholes equation to find the value of a european call option,
using the closed-form solution
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cmath>
#include <ql/time/date.hpp>
#include <ql/settings.hpp>
#include <ql/time/calendars/target.hpp>
#include <ql/time/daycounters/actual365fixed.hpp>
#include <ql/option.hpp>
#include <ql/termstructures/yield/flatforward.hpp>
#include <ql/quotes/simplequote.hpp>
#include <ql/termstructures/volatility/equityfx/blackconstantvol.hpp>
#include <ql/pricingengines/vanilla/analyticeuropeanengine.hpp>
#include <ql/exercise.hpp>
#include <ql/processes/blackscholesprocess.hpp>
#include <ql/math/distributions/normaldistribution.hpp>



using namespace QuantLib;

double calculateSD(std::vector<double> data)
{
    double sum{std::accumulate(data.begin(), data.end(), 0.0)};
    double mean{sum / data.size()};

    double sq_sum{std::inner_product(data.begin(), data.end(), data.begin(), 0.0)};
    double stdev{std::sqrt(sq_sum / data.size() - mean * mean)};

    return stdev;
}


double calculateVolatility(const std::vector<double>& prices) {
    // Step 1: Calculate logarithmic returns
    std::vector<double> returns;
    for (size_t i = 1; i < prices.size(); ++i) {
        double logReturn = std::log(prices[i] / prices[i - 1]);
        returns.push_back(logReturn);
    }

    // Step 2: Calculate standard deviation
    double standardDeviation = calculateSD(returns);

    // Step 3: Annualize standard deviation
    double annualizedVolatility = standardDeviation * std::sqrt(252.0);

    return annualizedVolatility;
}

std::vector<double> getVar(std::ifstream& data) {
    std::vector<double> res ; 
    std::string line;
    std::getline(data, line);  // Skip the header line

    std::vector<double> closingPrices;

    while (std::getline(data, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        std::vector<std::string> row;

        while (std::getline(lineStream, cell, ',')) {
            row.push_back(cell);
        }


        closingPrices.push_back(std::stod(row[4]));  // Close price is at index 4
    }

    res.push_back(closingPrices[0]);

    data.close(); 

    double var {calculateVolatility(closingPrices)};
    res.push_back(var);
    return res ; 
}

double calculateCallOptionPrice(double underlyingPrice, double strikePrice, double riskFreeRate, double volatility, double T) {
    // Set up the date settings
    Calendar calendar{TARGET()};
    DayCounter dayCounter{Actual365Fixed()};
    Date todaysDate{Date::todaysDate()};
    Settings::instance().evaluationDate() = todaysDate;

    // Set up the option parameters
    Option::Type type{Option::Call};
    Real underlying{underlyingPrice};
    Real strike{strikePrice};
    Spread dividendYield{0.00};
    Rate r{riskFreeRate};
    Volatility v{volatility};
    Date maturity{todaysDate + int(T * 365)};
    std::cout << "Maturity: " << maturity << std::endl;

    // Set up the processes
    Handle<Quote> underlyingH{boost::make_shared<SimpleQuote>(underlying)};
    Handle<YieldTermStructure> dividendYieldH{boost::make_shared<FlatForward>(todaysDate, dividendYield, dayCounter)};
    Handle<YieldTermStructure> riskFreeRateH{boost::make_shared<FlatForward>(todaysDate, r, dayCounter)};
    Handle<BlackVolTermStructure> volatilityH{boost::make_shared<BlackConstantVol>(todaysDate, calendar, v, dayCounter)};

    boost::shared_ptr<StrikedTypePayoff> payoff{new PlainVanillaPayoff(type, strike)};
    boost::shared_ptr<Exercise> europeanExercise{new EuropeanExercise(maturity)};

    VanillaOption EuropeanOption{payoff, europeanExercise};

    // Set up the Black-Scholes-Merton process
    boost::shared_ptr<BlackScholesMertonProcess> bsmProcess{new BlackScholesMertonProcess(underlyingH, dividendYieldH, riskFreeRateH, volatilityH)};


    /* The AnalyticEuropeanEngine uses the closed-form solution of the Black-Scholes equation */
    EuropeanOption.setPricingEngine(boost::make_shared<AnalyticEuropeanEngine>(bsmProcess));


    return EuropeanOption.NPV();
}



int main() {
    std::ifstream data("/home/seanf/quant/data/VOO.csv");
    if (!data.is_open()) {
        std::cout << "Error opening file" << std::endl;
        return 1;
    }
    std::vector<double> var{getVar(data)};
    double underlyingPrice{416.75};
    double strikePrice{430};
    double riskFreeRate{0.0549};
    double volatility{var[1]};
    double maturity = {0.4675};  // in years
    std::cout << "Maturity: " << maturity << std::endl;
    double callOptionPrice = calculateCallOptionPrice(underlyingPrice, strikePrice, riskFreeRate, volatility, maturity);

    std::cout << "Current Price: " << underlyingPrice << std::endl;
    std::cout << "Strike Price: " << strikePrice << std::endl;
    std::cout << "Call option price: " << callOptionPrice << std::endl;

    return 0;
}
