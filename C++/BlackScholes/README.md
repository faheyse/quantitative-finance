The only file which needs to be run is *run.sh*

for clean build uncomment the `rm` lines in run.sh

exporting env var `CMAKE_TOOLCHAIN_FILE` to the vcpkg.cmake file does NOT work.
It must be added as an option:
    cmake -DCMAKE_TOOLCHAIN_FILE=/home/seanf/vcpkg/scripts/buildsystems/vcpkg.cmake ..
