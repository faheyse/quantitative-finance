#include <ql/quotes/simplequote.hpp>
#include <ql/handle.hpp>
#include <ql/termstructures/yieldtermstructure.hpp>
#include <ql/termstructures/yield/flatforward.hpp>
#include <ql/termstructures/volatility/equityfx/blackconstantvol.hpp>
#include <ql/processes/blackscholesprocess.hpp>
#include <ql/pricingengines/vanilla/mceuropeanengine.hpp>
#include <ql/exercise.hpp>
#include <ql/instruments/vanillaoption.hpp>
#include <ql/payoff.hpp>
#include <ql/time/calendars/target.hpp>
#include <ql/time/daycounters/actual365fixed.hpp>
#include <ql/types.hpp>
#include <boost/shared_ptr.hpp>
#include <iostream>


using namespace QuantLib;

int main() {
    Settings::instance().evaluationDate() = Date::todaysDate() ; 

    // Option params
    Real underlying = 36;
    Real strike = 40;
    Spread dividendYield = 0.00;    // non- dividend paying stock
    Rate riskFreeRate = 0.06;
    Volatility volatility = 0.20;
    Date maturity(17, May, 2024);

    boost::shared_ptr<SimpleQuote> underlyingQuote(new SimpleQuote(underlying));
    RelinkableHandle<Quote> underlyingH(underlyingQuote);

    Handle<YieldTermStructure> riskFreeRateHandle(boost::shared_ptr<YieldTermStructure>(new FlatForward(0, TARGET(), riskFreeRate, Actual365Fixed())));
    Handle<YieldTermStructure> dividendYieldHandle(boost::shared_ptr<YieldTermStructure>(new FlatForward(0, TARGET(), dividendYield, Actual365Fixed())));

    Handle<BlackVolTermStructure> volatilityHandle(boost::shared_ptr<BlackVolTermStructure>(new BlackConstantVol(0, TARGET(), volatility, Actual365Fixed())));

    boost::shared_ptr<BlackScholesMertonProcess> process(new
        BlackScholesMertonProcess(underlyingH,
                                  dividendYieldHandle,
                                  riskFreeRateHandle,
                                  volatilityHandle));

    // Monte Carlo
    Size timeSteps = Null<Size>() ; 
    Size timeStepsPerYear = 360;
    bool brownianBridge = true;
    bool antitheticVariate = true;
    std::cout << "Antithetic Variate: " << (antitheticVariate ? "true" : "false") << std::endl;
    Size requiredSamples = -1;   // -1 means use required tolerance instead
    BigNatural seed = 42;
    Real requiredTolerance = 0.02;
    Size maxSamples = 1000000;

    boost::shared_ptr<PricingEngine> mcengine(new
        MCEuropeanEngine<PseudoRandom>(process,
                                    timeSteps,
                                    timeStepsPerYear,
                                    brownianBridge,
                                    antitheticVariate,
                                    requiredSamples,
                                    requiredTolerance,
                                    maxSamples,
                                    seed));


    // Set up the option and calculate the discounted option premium
    // European- style call option
    boost::shared_ptr<Exercise> europeanExercise(new EuropeanExercise(maturity));
    boost::shared_ptr<StrikedTypePayoff> payoff(new PlainVanillaPayoff(Option::Call, strike));
    VanillaOption europeanOption(payoff, europeanExercise);
    europeanOption.setPricingEngine(mcengine);

    std::cout << "The option PV is " << europeanOption.NPV() << std::endl;

    return 0;
}
