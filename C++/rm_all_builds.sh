#!/bin/bash

original_dir=$(pwd)

for dir in ./*; do
  if [ -d "$dir" ]; then
    cd "$dir" || continue

    if [ -f "rm_build.sh" ]; then
      echo "Executing $dir/rm_build.sh..."
      bash "rm_build.sh"
    else
      echo "No rm_build.sh found in $dir"
    fi

    cd "$original_dir" || exit
  fi
done

echo "Done"
