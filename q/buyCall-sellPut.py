import matplotlib.pyplot as plt
import numpy as np

# Parameters
strike_price = 110
premium = 1

# Range of stock prices at maturity
stock_prices_at_maturity = np.linspace(80, 140, 100)

# Calculate return for call option (buy)
call_returns = np.maximum(stock_prices_at_maturity - strike_price, 0) - premium

# Calculate return for put option (sell)
put_returns = premium - np.maximum(strike_price - stock_prices_at_maturity, 0)

# Calculate total return
total_returns = call_returns + put_returns

# Create plot
plt.figure(figsize=(10,6))
plt.plot(stock_prices_at_maturity, call_returns, label='Call Option Return')
plt.plot(stock_prices_at_maturity, put_returns, label='Put Option Return')
plt.plot(stock_prices_at_maturity, total_returns, label='Total Return', linewidth=2.5)
plt.xlabel('Stock Price at Maturity')
plt.ylabel('Return')
plt.title('Return vs. Stock Price at Maturity for a Portfolio of Buying a Call and Selling a Put')
plt.legend()
plt.grid(True)
plt.show()
