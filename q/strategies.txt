strategy 1:     (blackjack1.q)
    below MIN_VALUE: hit
        MIN_VALUE: 18
        result: ~43.5% win rate

        MIN_VALUE: 17
        result: ~45.5% win rate

        MIN_VALUE: 16
        result: ~45.7% win rate



betting strategy:
    martingale: increase bet after loss, reset bet to initial after win