plt.plot(stock_prices_at_maturity, call_returns, label='Call Option Return')
plt.plot(stock_prices_at_maturity, put_returns, label='Put Option Return')
plt.plot(stock_prices_at_maturity, stock_returns, label='Stock Return')