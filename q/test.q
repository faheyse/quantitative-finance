\c 1000 200  

system "S ",(string .z.i),(string first (sum "j"$string neg[.z.p]))

numbers: (til 10) + 1
faces: `jack`queen`king
suits: `clubs`diamonds`hearts`spades


shuffle: {[t] randIndex:(count t)?100000000; t: update randIndex from t; t: `randIndex xasc t; delete randIndex from t}

new_deck: { cards: cross[(numbers,faces);suits];
            t: flip (`card`suit!(flip cards));
            t: update index: til (count t) from t;
            t: update card_rank: ((4#(enlist 11 1)),asc {x,x,x,x}[(2+til 8)],((4*4)#10)) from t
        }

t: new_deck[]

bust: 0b        
player_bust: 0b 

getDealt: {[t] `t set shuffle[t] ; r:(1?(count t))(0) ; tmp: t(r); `t set (delete from t where index=r); (cols tmp)!(enlist each value tmp)}
      
deal: {delete index from flip getDealt[t],'getDealt[t]}

score: {[hand] sum hand[`card_rank]}
hit: {[hand] n: (flip (flip hand),'flip (delete index from flip getDealt[t])); `bust set ?[(min score[n]) > 21; 1b; 0b] ; n}

final: {[hand] ?[(max score[hand])>21; min score[hand]; max score[hand]]}

// deal & player are using the same strategy
// both want at least a soft 17, or else they hit
// i.e, until they have a score between 17 and 21, they continue to hit

decide: {while[(not (any (score[player] < 22)&(score[player]>(MIN_VALUE-1)))) & (not bust); `player set hit[player]]; `player_bust set bust; `bust set 0b; player;}

flip_hole: {while[(not (any (score[house] < 22)&(score[house]>16))) & (not bust); `house set hit[house]; house;]}


win_lose: {res: ?[final[player]>final[house]; `W ; `L]; res: ?[final[player]=final[house]; `T ; res];
           res: ?[bust=1b; `W; res];  res: ?[player_bust=1b; `L; res]; `player_bust set 0b; `bust set 0b; res
        } 


round: {`t set new_deck[]; `player set deal[]; `house set deal[]; decide[]; flip_hole[]; win_lose[]}

natural: {[hand] (final[hand]=21)&((count score[hand])>1)}

payout: {[res; bet; hand] p: ?[res=`W; 2*bet; 0]; p: ?[natural[hand]; p+(0.5*bet); p]; p}


MIN_VALUE: 17
num_simulations: 10000;
initial: 10
bet: 10
total_earnings: 0
do[     num_simulations;
        r: round[];
        cash: payout[r; bet; player];
        total_earnings: total_earnings+cash-bet;
        if[r=`L; bet: bet*2];
        if[r=`W; bet: initial];
        // add quit mechanism after certain limits??
 ]

show total_earnings


/perm:{(1 0#x){raze(1 rotate)scan'x,'y}/x}
/ perm[0 0 1 11][til 4]
/ (`clubs`diamonds`hearts`spades)!(perm[0 0 1 11][til 4])