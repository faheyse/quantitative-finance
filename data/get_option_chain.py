import yfinance as yf
import pandas as pd
from datetime import datetime

def get_option_chain(ticker, exp_date):
    stock = yf.Ticker(ticker)
    options = stock.option_chain(date=exp_date)
    
    calls = options.calls
    puts = options.puts
    
    return calls, puts

def main():
    ticker = "NVDA"
    
    stock = yf.Ticker(ticker)
    
    # Get the next 15 expiration dates
    exp_dates = stock.options[:50]
    
    # Initialize empty DataFrames to store call and put data
    all_calls = pd.DataFrame()
    all_puts = pd.DataFrame()

    # Iterate over expiration dates and collect call and put data
    for exp_date in exp_dates:
        calls, puts = get_option_chain(ticker, exp_date)
        
        # Drop the 'volume' and 'contractSize' columns as they sometimes have empty values
        calls = calls.drop(columns=['volume', 'contractSize', 'openInterest'])
        puts = puts.drop(columns=['volume', 'contractSize', 'openInterest'])
        
        # Add an expiration date column to each DataFrame
        calls['expirationDate'] = datetime.strptime(exp_date, '%Y-%m-%d')
        puts['expirationDate'] = datetime.strptime(exp_date, '%Y-%m-%d')
        
        # Append to the master DataFrames
        all_calls = pd.concat([all_calls, calls], ignore_index=True)
        all_puts = pd.concat([all_puts, puts], ignore_index=True)

    # Save the data to CSV files
    all_calls.to_csv(f"{ticker}_calls.csv", index=False)
    all_puts.to_csv(f"{ticker}_puts.csv", index=False)

    print(f"All call options saved to {ticker}_all_calls.csv")
    print(f"All put options saved to {ticker}_all_puts.csv")

if __name__ == "__main__":
    main()
