#!/bin/bash

# Database credentials
DBNAME="marketdb"
DBUSER="seanf"
DBHOST="localhost"
DBPORT="5432"

# CSV file paths
CALL_CSV="NVDA_calls.csv"

# Table name (with double quotes included)
TABLE_NAME="\"NVDA_CALL_CHAIN\""
echo $TABLE_NAME

# Create table
psql -U $DBUSER -d $DBNAME -h $DBHOST -p $DBPORT -c "DROP TABLE IF EXISTS $TABLE_NAME;"
psql -U $DBUSER -d $DBNAME -h $DBHOST -p $DBPORT -c "CREATE TABLE $TABLE_NAME (
    contractSymbol TEXT,
    lastTradeDate TIMESTAMP,
    strike NUMERIC,
    lastPrice NUMERIC,
    bid NUMERIC,
    ask NUMERIC,
    change NUMERIC,
    percentChange NUMERIC,
    impliedVolatility NUMERIC,
    inTheMoney BOOLEAN,
    currency TEXT,
    expirationDate DATE
);"

# Load CSV into table specifying the columns to copy from the CSV
psql -U $DBUSER -d $DBNAME -h $DBHOST -p $DBPORT -c "\COPY $TABLE_NAME(contractSymbol, lastTradeDate, strike, lastPrice, bid, ask, change, percentChange, impliedVolatility, inTheMoney, currency, expirationDate) FROM '$CALL_CSV' DELIMITER ',' CSV HEADER;"

# Notify the user
echo "Data has been successfully imported into $TABLE_NAME"
