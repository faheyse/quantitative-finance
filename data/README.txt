# To connect to database
psql -d marketdb  


# load.sh is a script to load data into database from SP500 (unzip the file first)

# postgresql user seanf has no password;
# must change authentication method in pg_hba.conf to trust
sudo vim /etc/postgresql/15/main/pg_hba.conf
    # IPv4 local connections:
    host    all             seanf           127.0.0.1/32            trust

# restart postgresql
sudo service postgresql restart


