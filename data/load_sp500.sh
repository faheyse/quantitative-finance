# load the SP500 data into the database

#!/bin/bash

DIR="SP500"
if [ $# -eq 1 ]; then
DIR=$1
fi

cd $DIR

for file in *.csv; do
  # Extract ticker name from filename
  ticker=$(basename -- "$file" .csv)

  # Connect to PostgreSQL and create a table for this ticker
  # Escaping the quotes around the ticker name
  psql -d marketdb -c "CREATE TABLE \"${ticker}\" (Date DATE, Open FLOAT, High FLOAT, Low FLOAT, Close FLOAT, Adj_Close FLOAT, Volume INT);"

  # Import CSV data into the newly created table
  # Escaping the quotes around the ticker name
  psql -d marketdb -c "\COPY \"${ticker}\" FROM '$file' WITH CSV HEADER;"
done



cd ..